console.log("Hello!");

let cubeNum = 2 ** 3;
console.log("The cube of 2 is" +" " + cubeNum);

let street="258 Washington Ave NW";
let state="California";
let code="90011";

const myAddress=` I live at ${street}, ${state},${code}.`
console.log(myAddress);

/*7. Create a variable animal with a
 value of an object data type with different animal details as it’s properties*/

 const animal = {
	name: "Lolong",
	kind: "saltwater crocodile",
	weight: "1075kg",
	length: "20ft 3in"
}

function myAnimal( { name, kind, weight, length } ){
	console.log(`${name} was a  ${kind}. He weighed at ${weight} with a measurement of ${length}.`);
	
};

myAnimal(animal);

/*9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the 
implicit return statement to print out the numbers.*/

const number = [1,2,3,4,5 ];
// pre-arrow function

number.forEach(function(numbers){
	console.log(`${numbers}`);
});

/*11. Create a variable reduceNumber and using the reduce array method and an 
arrow function console log the sum of all the numbers in the array.*/




const numbers = [1,2,3,4,5 ];

let sum=0;
for(let n of numbers)
sum  +=n;
console.log(sum);
numbers.reduce((accumulator, currentvalue)=>{

});

/*12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.*/


class Dog{
	constructor (name, breed, age){
		this.name = name;
		this.breed = breed;
		this.age = age;
			}
		};

const myDog = new Dog();

myDog.name = "Frankie";
myDog.breed = "Miniature Dachshund";
myDog.age = 5;


console.log(myDog);